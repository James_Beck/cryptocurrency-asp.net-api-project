﻿using Microsoft.AspNetCore.Mvc;

namespace CryptoWebJS.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
