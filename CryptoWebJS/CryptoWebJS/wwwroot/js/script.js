﻿const baseurl = 'https://api.coinmarketcap.com/v1/ticker/?limit=2';

let listAllValues = (theUrl) => {

    let xmlHttp = new XMLHttpRequest();

    xmlHttp.onreadystatechange = ((theURL) => {

        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {

            let valueslist = document.getElementById('values');
            let result = JSON.parse(xmlHttp.responseText);

            if (result.length > 0) {
                for (let i = 0; i < result.length; i++) {

                    let listitems = document.createElement('li');
                    listitems.setAttribute('class', 'list-group-item');
                    listitems.innerHTML = result[i].id + " - " + result[i].symbol + " - " + result[i].rank + " - " + result[i].price_usd;
                    valueslist.appendChild(listitems);
                }
            }
        }
    }).bind(undefined, theUrl);

    xmlHttp.open("GET", theUrl, true);
    xmlHttp.send(null);
}

let getValues = () => {
    listAllValues(baseurl);
}